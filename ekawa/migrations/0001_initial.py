# Generated by Django 3.1.4 on 2020-12-27 09:25

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='DetectionRequest',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('image', models.ImageField(upload_to='img/requests')),
                ('location', models.CharField(blank=True, max_length=200, null=True)),
                ('created_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Measure',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('point', models.CharField(blank=True, max_length=200, null=True)),
                ('status', models.CharField(choices=[('HEALTHY', 'HEALTHY'), ('UNHEALTHY', 'UNHEALTHY')], default='NEW', max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='DetectionResponse',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('status', models.CharField(choices=[('HEALTHY', 'HEALTHY'), ('UNHEALTHY', 'UNHEALTHY')], default='NEW', max_length=50)),
                ('created_at', models.DateField(auto_now_add=True)),
                ('measure', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ekawa.measure')),
                ('request', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ekawa.detectionrequest')),
            ],
        ),
    ]
