from django.db import models
from django.contrib.auth.models import User
import uuid
# Create your models here.


class DetectionRequest(models.Model):
    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    image = models.ImageField(upload_to='images/requests')
    location = models.CharField(max_length=200, blank=True, null=True)
    created_by = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return str(self.id)


class Measure(models.Model):
    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    point = models.CharField(max_length=200, blank=True, null=True)
    statues = {
        ('HEALTHY', 'HEALTHY'),
        ('UNHEALTHY', 'UNHEALTHY'),
    }
    status = models.CharField(max_length=50, choices=statues, default='NEW')

    def __str__(self):
        return str(self.id)


class DetectionResponse(models.Model):
    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    request = models.ForeignKey(DetectionRequest, on_delete=models.CASCADE)
    statues = {
        ('HEALTHY', 'HEALTHY'),
        ('UNHEALTHY', 'UNHEALTHY'),
    }
    status = models.CharField(
        max_length=50, choices=statues, default='HEALTHY')
    created_at = models.DateField(auto_now_add=True, null=False, blank=False)

    def __str__(self):
        return str(self.id)
