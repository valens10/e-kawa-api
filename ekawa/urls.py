from django.urls import path
from ekawa.views import *

urlpatterns = [
    path('measures/', MeasureList.as_view(), name='measures_list'),
    path('measures/<slug:pk>', MeasureDetail.as_view(), name='measures_detail'),

    path('user-requests/', create_request, name='requests'),

    path('responses/', DetectionResponseList.as_view(), name='responses_list'),
    path('responses/<slug:pk>', DetectionResponseDetail.as_view(),
         name='responses_detail'),

    path('requests/', DetectionRequestList.as_view(), name='requests_list'),
    path('requests/<slug:pk>', DetectionRequestDetail.as_view(),
         name='requests_detail'),
]
