from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from users.serializers import UserSerializer

from ekawa.models import DetectionRequest, DetectionResponse, Measure


class MeasureSerializer(serializers.ModelSerializer):
    class Meta:
        model = Measure
        fields = ('__all__')


class DetectionRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = DetectionRequest
        fields = ('__all__')

    def to_representation(self, instance):
        serialized_data = super(DetectionRequestSerializer,
                                self).to_representation(instance)
        serialized_data['created_by'] = UserSerializer(
            instance.created_by).data
        return serialized_data


class DetectionResponseSerializer(serializers.ModelSerializer):
    class Meta:
        model = DetectionResponse
        fields = ('__all__')

    def to_representation(self, instance):
        serialized_data = super(DetectionResponseSerializer,
                                self).to_representation(instance)
        serialized_data['request'] = DetectionRequestSerializer(
            instance.request).data
        return serialized_data
