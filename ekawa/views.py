from rest_framework.permissions import IsAdminUser
from django.shortcuts import render
import pickle
from django.contrib.auth.models import User
from users.serializers import UserSerializer
from rest_framework import generics, mixins
from rest_framework.response import Response
from rest_framework.permissions import AllowAny, IsAuthenticated
from ekawa.models import Measure, DetectionResponse, DetectionRequest
from rest_framework.decorators import api_view, permission_classes
from.serializers import MeasureSerializer, DetectionRequestSerializer, DetectionResponseSerializer

# Create your views here.


@api_view(['POST', 'GET'])
@permission_classes([IsAuthenticated])
def create_request(request):
    image = request.data.get('image')
    location = request.data.get('location')
    user = request.user

    user_request = DetectionRequest(
        image=image,
        location=location,
        created_by=user
    )
    user_request.save()

    prediction = prediction_model(user_request.image)

    if prediction:
        # create a responses with the user requests
        user_response = DetectionResponse(
            request=user_request,
            status=prediction
        )
        user_response.save()

        measures = Measure.objects.filter(status=prediction)

        serializers_data = []
        if measures and len(measures) > 0:
            for i in measures:
                serializers_data.append(MeasureSerializer(i).data)
        print('measures....', serializers_data)

        response = {}
        response['category'] = prediction
        response['user_response'] = DetectionResponseSerializer(
            user_response).data
        response['measures'] = serializers_data

        return Response(response, status=200)

    return Response({"detail": "Something went wrong!"}, status=400)


def prediction_model(image):
    # x = [[image, ]]
    # coffee_model = pickle.load(open('coffee_model.sav', 'rb'))
    #prediction = coffee_model.predict(x)
    return 'HEALTHY'


class MeasureList(mixins.ListModelMixin,
                  mixins.CreateModelMixin,
                  generics.GenericAPIView):

    queryset = Measure.objects.all()
    serializer_class = MeasureSerializer
    # permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class MeasureDetail(mixins.
                    RetrieveModelMixin, mixins.UpdateModelMixin,
                    mixins.DestroyModelMixin, generics.GenericAPIView):

    queryset = Measure.objects.all()
    serializer_class = MeasureSerializer
    # permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class DetectionResponseList(mixins.ListModelMixin,
                            mixins.CreateModelMixin,
                            generics.GenericAPIView):

    queryset = DetectionResponse.objects.all()
    serializer_class = DetectionResponseSerializer
    # permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class DetectionResponseDetail(mixins.
                              RetrieveModelMixin, mixins.UpdateModelMixin,
                              mixins.DestroyModelMixin, generics.GenericAPIView):

    queryset = DetectionResponse.objects.all()
    serializer_class = DetectionResponseSerializer
    # permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class DetectionRequestList(mixins.ListModelMixin,
                           mixins.CreateModelMixin,
                           generics.GenericAPIView):

    queryset = DetectionRequest.objects.all()
    serializer_class = DetectionRequestSerializer
    # permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class DetectionRequestDetail(mixins.
                             RetrieveModelMixin, mixins.UpdateModelMixin,
                             mixins.DestroyModelMixin, generics.GenericAPIView):

    queryset = DetectionRequest.objects.all()
    serializer_class = DetectionRequestSerializer
    # permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)
