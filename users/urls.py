from django.urls import path
from users.views import UserList, UserDetail, login

urlpatterns = [
    path('users/', UserList.as_view(), name='users_list'),
    path('users/<slug:pk>', UserDetail.as_view(), name='users_detail'),

    path('login', login, name='login'),
]
